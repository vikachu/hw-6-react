import React from "react";
import PropTypes from "prop-types";
import moment from "moment";
import { faHeart, faEdit, faTrash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "../styles/Message.scss";

class Message extends React.Component {
  render() {
    const props = this.props;
    const message = props.message;
    const timeCreated = moment(message.createdAt).format("hh:mm");

    return (
      <div
        className={
          "message__block" +
          (props.currentUserId === message.userId ? " own" : "")
        }
      >
        <div className={"message__container"}>
          <div className="message__avatar-wrapper">
            <div className="message__avatar">
              <img src={message.avatar} alt="avatar"></img>
            </div>
          </div>

          <div className="message__text-wrapper">
            <div className="message__text">{message.text}</div>
            {props.editingMessageId === message.id ? (
              <textarea
                className="message__text-edit"
                value={props.editInput}
                onChange={props.onEditMessageInputChange}
                onKeyDown={props._handleKeyDown}
              ></textarea>
            ) : (
              ""
            )}
            <div className="message__bottom-wrapper">
              {message.editedAt !== "" ? (
                <div className="message__edited">edited</div>
              ) : (
                ""
              )}
              <div className="message__time">{timeCreated}</div>
            </div>

            <div
              className={"message__like" + (message.likes > 0 ? " liked" : "")}
              onClick={() => this.props.onLikeClick(message)}
            >
              <FontAwesomeIcon icon={faHeart} />
              <div className="message__like__counter">{message.likes}</div>
            </div>
          </div>

          <div className="message__actions">
            <div
              className="message__actions-edit"
              onClick={() => this.props.onEditMessageClick(message)}
            >
              <FontAwesomeIcon icon={faEdit} />
            </div>

            <div
              className="message__actions-delete"
              onClick={() => this.props.onDeleteMessageClick(message)}
            >
              <FontAwesomeIcon icon={faTrash} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Message.propTypes = {
  message: PropTypes.objectOf(PropTypes.any).isRequired,
  currentUserId: PropTypes.string,
  onLikeClick: PropTypes.func.isRequired,
  onDeleteMessageClick: PropTypes.func.isRequired,
  onEditMessageClick: PropTypes.func.isRequired,
  editingMessageId: PropTypes.string,
  editInput: PropTypes.string,
  onEditMessageInputChange: PropTypes.func.isRequired,
  _handleKeyDown: PropTypes.func.isRequired,
};

export default Message;
