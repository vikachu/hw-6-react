import React from "react";
import "../styles/Logo.scss";
import { faRocket} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class Logo extends React.Component {
  render() {
    return (
      <div className="logo__container">
          <FontAwesomeIcon icon={faRocket} />
          <span>Rocket chat app</span>
      </div>
    );
  }
}

export default Logo;
