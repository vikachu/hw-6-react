import React from "react";
import PropTypes from "prop-types";
import "../styles/MessageList.scss";
import moment from "moment";

import Message from "./Message";
import SeparatorLine from "./SeparatorLine";

class MessageList extends React.Component {
  render() {
    const props = this.props;
    const messages = props.messages;
    let messageDate = moment(messages[0].createdAt);

    return (
      <div className="message-list__container">
        <SeparatorLine separatorDate={messageDate.format("DD MMM")} />
        {messages.map((message) => {
          const currentMessageDate = moment(message.createdAt);
          if (currentMessageDate.isAfter(messageDate, "day")) {
            messageDate = currentMessageDate.isSame(moment(), "day")
              ? "Today"
              : currentMessageDate.isSame(moment().subtract(1, "day"), "day")
              ? "Yesterday"
              : currentMessageDate.format("DD MMM");

            return (
              <div key={messageDate}>
                <SeparatorLine separatorDate={messageDate} key={messageDate} />
                <Message
                  key={message.id}
                  message={message}
                  currentUserId={props.currentUserId}
                  onLikeClick={props.onLikeClick}
                  onDeleteMessageClick={props.onDeleteMessageClick}
                  onEditMessageClick={props.onEditMessageClick}
                  editingMessageId={props.editingMessageId}
                  editInput={props.editInput}
                  onEditMessageInputChange={props.onEditMessageInputChange}
                  _handleKeyDown={props._handleKeyDown}
                />
              </div>
            );
          } else {
            return (
              <Message
                key={message.id}
                message={message}
                currentUserId={props.currentUserId}
                onLikeClick={props.onLikeClick}
                onDeleteMessageClick={props.onDeleteMessageClick}
                onEditMessageClick={props.onEditMessageClick}
                editingMessageId={props.editingMessageId}
                editInput={props.editInput}
                onEditMessageInputChange={props.onEditMessageInputChange}
                _handleKeyDown={props._handleKeyDown}
              />
            );
          }
        })}
      </div>
    );
  }
}

MessageList.propTypes = {
  message: PropTypes.objectOf(PropTypes.any),
  currentUserId: PropTypes.string,
  onLikeClick: PropTypes.func.isRequired,
  onDeleteMessageClick: PropTypes.func.isRequired,
  onEditMessageClick: PropTypes.func.isRequired,
  editingMessageId: PropTypes.string,
  editInput: PropTypes.string,
  onEditMessageInputChange: PropTypes.func.isRequired,
  _handleKeyDown: PropTypes.func.isRequired,
};

export default MessageList;
