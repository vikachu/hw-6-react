import React from "react";
import moment from "moment";
import "../styles/Chat.scss";

import Header from "./Header";
import MessageList from "./MessageList";
import MessageInput from "./MessageInput";
import LoadingSpinner from "./LoadingSpinner";
import Logo from "./Logo";
import Copyright from "./Copyright";

class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      editingMessageId: "",
      editInput: "",
      input: "",
      messages: [],
      currentUser: {
        id: null,
        name: null,
        avatar: null,
      },
    };
    this.onMessageInputChange = this.onMessageInputChange.bind(this);
    this.onSendMessageClick = this.onSendMessageClick.bind(this);
    this.onLikeClick = this.onLikeClick.bind(this);
    this.onDeleteMessageClick = this.onDeleteMessageClick.bind(this);
    this.onEditMessageClick = this.onEditMessageClick.bind(this);
    this.onEditMessageInputChange = this.onEditMessageInputChange.bind(this);
    this._handleKeyDown = this._handleKeyDown.bind(this);
  }

  componentDidMount() {
    fetch("https://edikdolynskyi.github.io/react_sources/messages.json")
      .then((res) => res.json())
      .then((res) => {
        return res.sort((a, b) => moment(a.createdAt).diff(b.createdAt));
      })
      .then(
        (result) => {
          const currentUser = result[0];

          this.setState({
            isLoaded: true,
            messages: result.map((message) => ({
              ...message,
              likes: 0,
            })),
            currentUser: {
              id: currentUser.userId,
              name: currentUser.user,
              avatar: currentUser.avatar,
            },
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error,
          });
        }
      );
  }

  async onMessageInputChange(e) {
    const value = e.target.value;
    await this.setState({
      ...this.state,
      input: value,
    });
  }

  async onSendMessageClick() {
    if (this.state.input !== "") {
      const message = {
        id: Math.random().toString().substr(2, 8),
        text: this.state.input,
        user: this.state.currentUser.name,
        avatar: this.state.currentUser.avatar,
        userId: this.state.currentUser.id,
        createdAt: moment(),
        editedAt: "",
        likes: 0,
      };
      await this.setState({
        ...this.state,
        messages: this.state.messages.concat(message),
        input: "",
      });
    }
  }

  async onLikeClick(clickMessage) {
    if (this.state.currentUser.id !== clickMessage.userId) {
      await this.setState({
        ...this.state,
        messages: this.state.messages.map((message) => ({
          ...message,
          likes:
            message.id === clickMessage.id ? message.likes + 1 : message.likes,
        })),
      });
    }
  }

  async onDeleteMessageClick(clickMessage) {
    if (this.state.currentUser.id === clickMessage.userId) {
      await this.setState({
        ...this.state,
        messages: this.state.messages.filter(
          (message) => message.id !== clickMessage.id
        ),
      });
    }
  }

  async onEditMessageClick(clickMessage) {
    if (this.state.currentUser.id === clickMessage.userId) {
      await this.setState({
        ...this.state,
        editInput: clickMessage.text,
        editingMessageId: clickMessage.id,
      });
    }
  }

  async onEditMessageInputChange(e) {
    const value = e.target.value;
    await this.setState({
      ...this.state,
      editInput: value,
    });
  }

  _handleKeyDown(e) {
    if (e.key === "Enter") {
      this.setState({
        ...this.state,
        messages: this.state.messages.map((message) =>
          message.id === this.state.editingMessageId
            ? {
                ...message,
                text: this.state.editInput,
                editedAt: moment()
              }
            : message
        ),
        editInput: "",
        editingMessageId: "",
      });
    }
  }

  render() {
    if (!this.state.isLoaded) {
      return <LoadingSpinner />;
    } else {
      const messages = this.state.messages;
      const userIds = new Set();
      messages.map((message) => userIds.add(message.userId));

      const lastMessageTime = moment(
        messages[messages.length - 1].createdAt
      ).format("hh:mm");

      return (
        <div>
          <div className="chat__container">
            <Logo />
            <Header
              messagesCount={messages.length}
              participantsCount={userIds.size}
              lastMessageTime={lastMessageTime}
            />
            <MessageList
              messages={messages}
              currentUserId={this.state.currentUser.id}
              onLikeClick={this.onLikeClick}
              onDeleteMessageClick={this.onDeleteMessageClick}
              onEditMessageClick={this.onEditMessageClick}
              editingMessageId={this.state.editingMessageId}
              editInput={this.state.editInput}
              onEditMessageInputChange={this.onEditMessageInputChange}
              _handleKeyDown={this._handleKeyDown}
            />
            <MessageInput
              onMessageInputChange={this.onMessageInputChange}
              onSendMessageClick={this.onSendMessageClick}
              inputValue={this.state.input}
            />
            <Copyright />
          </div>
        </div>
      );
    }
  }
}

export default Chat;
